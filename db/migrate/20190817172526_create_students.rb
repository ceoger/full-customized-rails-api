class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :course
      t.integer :batch
      t.integer :semester
      t.integer :roll_number
      t.string :phone_number
      t.string :country
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
