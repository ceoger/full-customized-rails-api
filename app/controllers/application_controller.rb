class ApplicationController < ActionController::API
  # protect_from_forgery with: :exception #learning
  # self.responder = ApplicationResponder #old server
  # respond_to :html #old server

  # before_action :authenticate_request! #old server
  # attr_reader :current_user #old server

  protected

  def is_a_valid_email?(email)
      (/\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i =~ email) != nil
    end

  def not_found
    render json: { error: 'not_found' }
  end

  # oldserver
  def authorize(level)
      User.access_levels[@current_user.access_level] <= User.access_levels[level]
    end

  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      @current_user = User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end

  # load_current_user!
  #   invalid_authentication unless @current_user
  # end

  def invalid_authentication
    render json: {error: 'Invalid Request'}, status: :unauthorized
  end

  def invalid_authorization
    render json: {error: 'Not Authorized'}, status: :unauthorized

  end

  # oldsever
  def load_current_user!
      @current_user = User.find_by(id: payload[0]['user_id'])
    end
end
