class UsersController < ApplicationController
  before_action :authorize_request, except: :create
  before_action :find_user, except: %i[create index]
  before_action :set_user, only: [:show, :update, :destroy]


  # GET /users
  def index
    @users = User.all
    render json: @users, status: :ok
  end

  # GET /users/{username}
  def show
    render json: @user, status: :ok
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :created
    else
      render json: { errors: @user.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  # PUT /users/{username}
  def update
    unless @user.update(user_params)
      render json: { errors: @user.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  # DELETE /users/{username}
  def destroy
    @user.destroy
  end
  #oldserver
  # def get_user_info
  #
  #   if is_a_valid_email?(params[:email])
  #     @user = User.find_by(email: params[:email])
  #   else
  #     @user = User.find_by(username: params[:email])
  #   end
  #   render json: @user
  # end

  private
  def set_user
      @user = User.find(params[:id])
    end

  def find_user
    @user = User.find_by_username!(params[:_username])
    rescue ActiveRecord::RecordNotFound
      render json: { errors: 'User not found' }, status: :not_found
  end

  def user_params
    params.permit(
      :find_user, :last_name, :username, :email, :password, :password_confirmation
    )
  end
end
