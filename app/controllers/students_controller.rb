class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :update, :destroy]

  # GET /students
  # GET /students.json
  def index
    @students = current_user.student.all

    render json: @students
  end

  # GET /students/1
  # GET /students/1.json
  def show
    render json: @student
  end

  # POST /students
  # POST /students.json
  def create

    # @student = current_user.students.build(student_params)

    if (params[:user_id] != @current_user.id)
      render json: @user_detail.errors, status: :unprocessable_entity
      return
    end

    @student = Student.new(student_params)

    if @student.save
      render :show, status: :created, location: @student
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    if @student.update(student_params)
      render :show, status: :ok, location: @student
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = current_user.students.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:first_name, :last_name, :email, :course, :batch, :semester, :roll_number, :phone_number, :country, :user_id)
    end
end
