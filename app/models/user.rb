class User < ApplicationRecord
  before_save { self.email = email.downcase }
  attr_accessor :activation_token
  before_save   :downcase_email
  before_create :create_activation_digest

  has_secure_password
  # mount_uploader :avatar, AvatarUploader
  validates :email, presence: true, uniqueness: { message: "Email already exist."}
  validates :email, format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i, message: "Please provide a valid email address." }
  validates :username, presence: true, uniqueness: {message: "username is already taken"}
  validates :password,
            length: { minimum: 6 },
  if: -> { new_record? || !password.nil? }

    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

end
