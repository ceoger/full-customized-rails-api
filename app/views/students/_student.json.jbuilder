json.extract! student, :id, :first_name, :last_name, :email, :course, :batch, :semester, :roll_number, :phone_number, :country, :user_id, :created_at, :updated_at
json.url student_url(student, format: :json)
