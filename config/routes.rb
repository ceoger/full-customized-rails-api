Rails.application.routes.draw do
  resources :students
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users, param: :_username
  resources :account_activations, only: [:edit]
  post '/auth/login', to: 'authentication#login'
  # post 'get_user_info', to: 'users#get_user_info'
  get '/*a', to: 'application#not_found'
  root to: 'home#index'
end
